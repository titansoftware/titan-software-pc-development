from distutils.core import setup
import py2exe

Mydata_files = [('downloadedwidgets.txt', ['/downloadedwidgets.txt']), ('easygui.py', ['/easygui.py'])]

setup(
    console=['titansoftware.py'],
    data_files = Mydata_files,
                 "py2exe":{
                         "unbuffered": True,
                         "optimize": 2,
                         "excludes": ["email"]
                 }
         }
)
