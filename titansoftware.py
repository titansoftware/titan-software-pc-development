# Titan Software is licenced under a Creative Commons Attribution-NoDerivatives 4.0 International Licence:
# http://creativecommons.org/licences/by-nd/4.0/

# -start code-
import easygui, urllib, pickle

# get widget list
widgetlist = []
widgetfile = open('downloadedwidgets.txt', 'rb')
widgetlist_basic = widgetfile.readlines()
for i in range(len(widgetlist_basic)):
    if widgetlist_basic[i].endswith("\n"):
        widgetlist.append(widgetlist_basic[i].strip("\n"))
    else:
        pass
widgetfile.close()

# welcome users
easygui.msgbox("Welcome to Titan Software.", 'Titan Software')

# main menu function
def mainmenu():

    # opening choice
    openingchoice = easygui.buttonbox("What would you like to do?", "Titan Software", ["Add a widget", "View my widgets", "Create a widget", "Cancel"])

    # add widget
    if openingchoice == "Add a widget":
        widgetfilewrite = open('downloadedwidgets.txt', 'wb')
        url2add = easygui.enterbox("What is the url of the widget?", 'Titan Software')
        try:
            widgetfilewrite.write(url2add + '\n')
            widgetfilewrite.close()
            easygui.msgbox('Your widget hs been downloaded. You will need to re-open Titan Software to be able to select the widget.', 'Titan Software')
        except:
                  easygui.msgbox("Oh no! Something went wrong.\nWe would really appreciate it if you could give us some feedback at:\nhttp://titansoftware.bitbucket.org/contacts.html")

    # view widgets
    elif openingchoice == "View my widgets":
        selwidget = easygui.choicebox('Choose a widget to open:', 'TitanSoftware', widgetlist)
        if selwidget != '':
            widgetcodelink = urllib.urlopen(selwidget)
            widgetcodestring = str(widgetcodelink.read())
            exec widgetcodestring
            returnto()
        else:
            returnto()

    # create a widget
    elif openingchoice == "Create a widget":
        easygui.msgbox("Go to this website: http://titansoftware.bitbucket.org/createwidget.html/", 'Titan Software')
        returnto()

    # else = quit
    else:
        quit()

# go back to main menu
def returnto():
    try:
        mainmenu()
    except:
        easygui.msgbox("Oh no! Something went wrong.\nWe would really appreciate it if you could give us some feedback at:\nhttp://titansoftware.bitbucket.org/contacts.html")
    
# run the app
mainmenu()
